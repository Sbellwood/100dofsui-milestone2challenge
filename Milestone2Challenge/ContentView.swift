//
//  ContentView.swift
//  Milestone2Challenge
//
//  Created by Skyler Bellwood on 7/8/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var isGameInProgress = false
    @State private var isAlertShowing = false
    @State private var score = 0
    
    @State private var multiplierDifficulty = 10
    @State private var questionsSelection = 2
    let questionsNumber = ["5", "10", "15", "20", "All"]
    
    @State private var questionBank = [Question]()
    @State private var currentQuestion = 0
    @State private var currentAnswer = ""
    @State private var alertTitle = "You answered all the questions!"
    @State private var alertMessage = ""
    
    var body: some View {
        NavigationView {
            Group {
                if isGameInProgress {
                    Form {
                        Section(header: Text("Question:")) {
                            Text("\(questionBank[currentQuestion].text)")
                        }
                        
                        Section(header: Text("Answer:")) {
                            TextField("0", text: $currentAnswer)
                        }
                        
                        Section {
                            Button("Submit") {
                                self.checkAnswer()
                            }
                        }
                        
                        Section {
                            Text("Current score: \(score)")
                        }
                     }
                    .alert(isPresented: $isAlertShowing) {
                        Alert(title: Text(alertTitle), message: Text(alertMessage), dismissButton: .default(Text("OK")) {
                            self.isGameInProgress.toggle()
                        })
                    }
                } else {
                    Form {
                        Section(header: Text("Multiplier Selection")) {
                            Stepper("\(multiplierDifficulty)", value: $multiplierDifficulty, in: 1...12)
                        }
                        Section(header: Text("Number of Questions")) {
                            Picker("\(questionsNumber[questionsSelection])", selection: $questionsSelection) {
                                ForEach(0..<questionsNumber.count) {
                                    Text("\(self.questionsNumber[$0])")
                                }
                                .navigationBarTitle("Number of Questions")
                            }
                            .navigationBarTitle("Swifty Multiply")
                        }
                        Section {
                            Button("Start Game") {
                                self.populateQuestionBank()
                                self.isGameInProgress.toggle()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func resetGame() {
        alertMessage = "\(score) out of \(questionsNumber[questionsSelection]) correct"
        isAlertShowing.toggle()
        score = 0
    }
    
    func checkAnswer() {
        if Int(currentAnswer) ?? 0 == questionBank[currentQuestion].answer {
            score += 1
        }
        
        self.currentAnswer = ""
        currentQuestion += 1
        
        if questionBank.endIndex == currentQuestion {
            currentQuestion = 0
            resetGame()
        }
    }
    
    func populateQuestionBank() {
        if questionsSelection == 4 {
            for x in 1...multiplierDifficulty {
                for y in 1...multiplierDifficulty {
                    questionBank.append(Question(multiplier1: x, multiplier2: y))
                }
            }
            questionBank.shuffle()
        } else {
            guard let numberOfDesiredQuestions = Int(questionsNumber[questionsSelection]) else {
                return
            }
            
            for _ in 1...numberOfDesiredQuestions {
                questionBank.append(Question(multiplier1: Int.random(in: 1...multiplierDifficulty), multiplier2: Int.random(in: 1...multiplierDifficulty)))
            }
        }
    }
}

struct Question {
    let answer: Int
    let text: String
    
    init(multiplier1: Int, multiplier2: Int) {
        answer = multiplier1 * multiplier2
        text = "\(multiplier1) X \(multiplier2)"
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
